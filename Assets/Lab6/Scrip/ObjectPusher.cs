using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Korawan.GameDev3.Chapter6.UnityEvents
{
    public class ObjectPusher : MonoBehaviour
    {
        // Start is called before the first frame update
        [SerializeField] private float _forceMagnitude = 10;

        private Rigidbody _rigidbody;
        
        private void Start()
        {
            _rigidbody = GetComponent<Rigidbody>();
        }

        public void Push(GameObject actor)
        {
            _rigidbody.AddForce(actor.transform.forward*_forceMagnitude,ForceMode.Impulse);
        }
    }
}