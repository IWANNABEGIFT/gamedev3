using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Korawan.GameDev3.Chapter1
{
    public enum ItemType
    {
        COIN,
        BIGCOIN,
        POWERUP,
        POWERDOWN,
        C1,
        C2,
    }

    public class NewBehaviourScript : MonoBehaviour
    {
        // Start is called before the first frame update
        void Start()
        {

        }

        // Update is called once per frame
        void Update()
        {

        }
    }
}
