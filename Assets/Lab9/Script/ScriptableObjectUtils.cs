using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Korawan.GameDev3.Chapter9.Interaction
{
    [CreateAssetMenu(menuName = "GameDev3/Util/ScriptableObjectUtils")]

    public class ScriptableObjectUtils : ScriptableObject
    {
        // Start is called before the first frame update
        public void Destroy(GameObject gameObject)
        {
            Object.Destroy(gameObject);
        }
    }
}