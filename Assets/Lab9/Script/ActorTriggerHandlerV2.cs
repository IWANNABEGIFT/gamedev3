using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Korawan.GameDev3.Chapter5.InteractionSystem;

namespace Korawan.GameDev3.Chapter9.Interaction
{

    public class ActorTriggerHandlerV2 : ActorTriggerHandler
    {
        // Start is called before the first frame update
        public virtual IInteractable[] GetInteractables()
        {
            m_TriggeredGameObjects.RemoveAll(gameject => gameject == null);

            if (m_TriggeredGameObjects.Count == 0)
            {
                return null;
            }
            return m_TriggeredGameObjects[0].GetComponents <IInteractable >();
        }
    }
}