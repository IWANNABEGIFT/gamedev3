using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Korawan.GameDev3.Chapter5.InteractionSystem;
using UnityEngine.InputSystem;

namespace Korawan.GameDev3.Chapter5.PlayerController
{
    public class Ch5CapsulePlayerControllerWithActorTrigger : Ch5CapsulePlayerControllerWithPreset
    {
        // Start is called before the first frame update
        [SerializeField] protected ActorTriggerHandler m_ActorTriggerHandler;

        protected override void Update()
        {
            base.Update();
            
            Keyboard keyboard = Keyboard.current;

            if (keyboard[m_Preset.InteractionKey].wasPressedThisFrame)
            {
                PerformInteraction();
            }
        }

        protected virtual void PerformInteraction()
        {
            var interactable = m_ActorTriggerHandler.GetInteractable();

            if (interactable != null)
            {
                interactable.Interact(gameObject);
            }
        }
    }
}