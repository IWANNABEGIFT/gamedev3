using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Korawan.GameDev3.Chapter5.InteractionSystem
{

    public interface IActorEnterExitHandler
    {
        // Start is called before the first frame update
        void ActorEnter(GameObject actor);
        void ActorExit(GameObject actor);
    }
}